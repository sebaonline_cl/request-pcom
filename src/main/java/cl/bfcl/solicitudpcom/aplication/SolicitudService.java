/**
 * 
 */
package cl.bfcl.solicitudpcom.aplication;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.bfcl.solicitudpcom.domain.Solicitud;
import cl.bfcl.solicitudpcom.infraestructure.ISolicitudRepository;

/**
 * @author ggonzalezh
 *
 */
@Service
public class SolicitudService {
	
	private ISolicitudRepository solicitudRepository;

	@Autowired
	public SolicitudService(ISolicitudRepository solicitudRepository) {
		this.solicitudRepository = solicitudRepository;
	}
	
	/**
	 * Metodo para Buscar un cliente por su identificacion
	 * 
	 * @param identificacionCli
	 * @return
	 */
	public Solicitud findByNroSolicitud(BigDecimal nroSolicitud) {
		return this.solicitudRepository.findByNroSolicitud(nroSolicitud);
	} 

}
