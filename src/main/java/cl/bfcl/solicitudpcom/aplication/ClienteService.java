package cl.bfcl.solicitudpcom.aplication;

import cl.bfcl.solicitudpcom.shared.vo.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ClienteService {

    public ClienteVO searchClientAccounts(){
        ClienteVO clienteVO = new ClienteVO();
        clienteVO = mockClient();
        return clienteVO;
    }

    private ClienteVO mockClient(){
        ClienteVO cliente = new ClienteVO();
        List<CuentaVO> cuentaCliente = new ArrayList<>();
        cuentaCliente = mapearCuenta();
        cliente.setRut(272138664);
        cliente.setNombres("Robert");
        cliente.setApellidoPaterno("Alvarado");
        cliente.setApellidoMaterno("Medina");
        cliente.setEmail("robert@falabella.com");
        cliente.setTelefonoContacto("123123123");
        cliente.setCuentas(cuentaCliente);
        return cliente;
    }

    private List<CuentaVO> mapearCuenta(){
        List<CuentaVO> cuentaCliente = new ArrayList<>();
        MonedaVO moneda = new MonedaVO();
        CuentaVO cuentaCorriente = new CuentaVO("1",
                "1-01",
                "10100016863",
                "CUENTA CORRIENTE",
                "2011-09-26",
                "2011-09-21",
                "0",
                mapeoMoneda(),
                mapeoSituacionVO(),
                6287346,
                6287346,
                "0"
                );
        CuentaVO cuentaVista = new CuentaVO("2",
                "2-02",
                "10100016864",
                "CUENTA VISTA",
                "2011-09-26",
                "2011-09-21",
                "0",
                mapeoMoneda(),
                mapeoSituacionVO(),
                62873,
                6287,
                "0"
        );
        cuentaCliente.add(cuentaCorriente);
        cuentaCliente.add(cuentaVista);
        return cuentaCliente;
    }

    private MonedaVO mapeoMoneda(){
        MonedaVO moneda = new MonedaVO();
        moneda.setMoneda(999);
        return moneda;
    }

    private SituacionVO mapeoSituacionVO(){
        SituacionVO situacionVO = new SituacionVO();
        situacionVO.setCodigo(1);
        situacionVO.setEstado(00);
        situacionVO.setGlosa("Vigente");
        return situacionVO;
    }
}
