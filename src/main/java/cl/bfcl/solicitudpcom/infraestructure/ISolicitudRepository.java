/**
 * 
 */
package cl.bfcl.solicitudpcom.infraestructure;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.bfcl.solicitudpcom.domain.Solicitud;

/**
 * @author ggonzalezh
 *
 */
@Repository
public interface ISolicitudRepository extends JpaRepository<Solicitud, BigDecimal> {

	Solicitud findByNroSolicitud(BigDecimal nroSolicitud);

}
