package cl.bfcl.solicitudpcom.ui;

import cl.bfcl.solicitudpcom.aplication.ClienteService;
import cl.bfcl.solicitudpcom.domain.Parametro;
import cl.bfcl.solicitudpcom.shared.vo.ClienteVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ralvaradom
 *
 */
@RestController
@RequestMapping("/hipotecario")
@Api(tags = "cliente-cuentas", description = "Servicio trae datos de cliente y sus cuentas asociadas")
public class ClienteRestController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping("/cliente-cuentas")
    @ApiOperation(value = "Busca el cliente con sus cuentas asociadas", nickname = "obtenerClienteCuentas",
            response = Parametro.class)
    public ResponseEntity<?> obtenerClienteCuenta() {
        ClienteVO clienteVO;
        Map<String, Object> response = new HashMap<>();
        try {
            clienteVO = clienteService.searchClientAccounts();
        } catch(DataAccessException e) {
            response.put("mensaje", "Error al realizar la consulta en la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(clienteVO, HttpStatus.OK);
    }
}
