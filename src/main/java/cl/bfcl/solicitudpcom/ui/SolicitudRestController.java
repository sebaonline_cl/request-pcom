/**
 * 
 */
package cl.bfcl.solicitudpcom.ui;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import cl.bfcl.solicitudpcom.aplication.SolicitudService;
import cl.bfcl.solicitudpcom.domain.ErrorRest;
import cl.bfcl.solicitudpcom.domain.Solicitud;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

/**
 * @author ggonzalezh
 *
 */
@RestController
@RequestMapping("/hipotecario/solicitud")
@Api(tags = "solicitud", description = "Solicitud de crédito hipotecario")
public class SolicitudRestController {

	private static Logger LOG = LoggerFactory.getLogger(SolicitudRestController.class);

	@Autowired
	private SolicitudService SolicitudService;
	private final HttpServletRequest request;

	public SolicitudRestController(SolicitudService solicitudService, HttpServletRequest request) {
		this.SolicitudService = solicitudService;
		this.request = request;
	}
	
	@GetMapping("/{solicitud}/legado")
	@ApiOperation(value = "Busca si la solicitud existe en PCOM", nickname = "obtenerSolicitud", 
		notes = "Enviar numero de id necesario busca en faban si existe ", 
		response = Solicitud.class, 
		authorizations = {
					@Authorization(value = "password", scopes = {
					@AuthorizationScope(scope = "", description = "")            })    })
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "OK", response = Solicitud.class),
			@ApiResponse(code = 400, message = "Error en la ruta del servicio", response = ErrorRest.class),
	        @ApiResponse(code = 401, message = "Error con la autenticación del servicio web", response = ErrorRest.class),
	        @ApiResponse(code = 403, message = "Error prohibido", response = ErrorRest.class),
	        @ApiResponse(code = 404, message = "Solicitud no existe", response = ErrorRest.class),
	        @ApiResponse(code = 500, message = "Error interno", response = ErrorRest.class)})	
	public ResponseEntity<?> obtenerSolicitud(@PathVariable("solicitud") BigDecimal solicitud)  {
		Map<String, Object> response = new HashMap<>();
		Solicitud rsolicitud = new Solicitud();
		try {
			rsolicitud = this.SolicitudService.findByNroSolicitud(solicitud);
			LOG.info("Solicitud {}", rsolicitud);
		} catch(DataAccessException e) {
			response.put("mensaje", "Solicitud no existe");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (rsolicitud == null) {
			response.put("nroSolicitud", 0);
			response.put("code", HttpStatus.NOT_FOUND);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} 
		LOG.info("Solicitud: ", rsolicitud.getNroSolicitud());
		return new ResponseEntity<Solicitud>(rsolicitud, HttpStatus.OK);

	}
}
