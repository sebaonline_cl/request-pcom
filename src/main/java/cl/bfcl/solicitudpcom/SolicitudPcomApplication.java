package cl.bfcl.solicitudpcom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class SolicitudPcomApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolicitudPcomApplication.class, args);
	}

}
