package cl.bfcl.solicitudpcom.shared.vo;

public class MonedaVO {

    private Integer moneda;

    public Integer getMoneda() {
        return moneda;
    }

    public void setMoneda(Integer moneda) {
        this.moneda = moneda;
    }
}
