package cl.bfcl.solicitudpcom.shared.vo;

public class CuentaVO {

    private String codigoProducto;
    private String codigoSubProducto;
    private String identificadorProducto;
    private String descripcionProducto;
    private String fechaActivacion;
    private String fechaCreacion;
    private String ejecutivoCreacion;
    MonedaVO moneda;
    SituacionVO situacion;
    private Number saldoContable;
    private Number saldoDisponible;
    private String motivoCierre;
    
    
    
	public CuentaVO(String codigoProducto, String codigoSubProducto, String identificadorProducto,
			String descripcionProducto, String fechaActivacion, String fechaCreacion, String ejecutivoCreacion,
			MonedaVO moneda, SituacionVO situacion, Number saldoContable, Number saldoDisponible, String motivoCierre) {
		this.codigoProducto = codigoProducto;
		this.codigoSubProducto = codigoSubProducto;
		this.identificadorProducto = identificadorProducto;
		this.descripcionProducto = descripcionProducto;
		this.fechaActivacion = fechaActivacion;
		this.fechaCreacion = fechaCreacion;
		this.ejecutivoCreacion = ejecutivoCreacion;
		this.moneda = moneda;
		this.situacion = situacion;
		this.saldoContable = saldoContable;
		this.saldoDisponible = saldoDisponible;
		this.motivoCierre = motivoCierre;
	}
	public String getCodigoProducto() {
		return codigoProducto;
	}
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	public String getIdentificadorProducto() {
		return identificadorProducto;
	}
	public void setIdentificadorProducto(String identificadorProducto) {
		this.identificadorProducto = identificadorProducto;
	}
	public String getDescripcionProducto() {
		return descripcionProducto;
	}
	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}
	public String getFechaActivacion() {
		return fechaActivacion;
	}
	public void setFechaActivacion(String fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getEjecutivoCreacion() {
		return ejecutivoCreacion;
	}
	public void setEjecutivoCreacion(String ejecutivoCreacion) {
		this.ejecutivoCreacion = ejecutivoCreacion;
	}
	public MonedaVO getMoneda() {
		return moneda;
	}
	public void setMoneda(MonedaVO moneda) {
		this.moneda = moneda;
	}
	public SituacionVO getSituacion() {
		return situacion;
	}
	public void setSituacion(SituacionVO situacion) {
		this.situacion = situacion;
	}
	public Number getSaldoContable() {
		return saldoContable;
	}
	public void setSaldoContable(Number saldoContable) {
		this.saldoContable = saldoContable;
	}
	public Number getSaldoDisponible() {
		return saldoDisponible;
	}
	public void setSaldoDisponible(Number saldoDisponible) {
		this.saldoDisponible = saldoDisponible;
	}
	public String getMotivoCierre() {
		return motivoCierre;
	}
	public void setMotivoCierre(String motivoCierre) {
		this.motivoCierre = motivoCierre;
	}

    
}