package cl.bfcl.solicitudpcom.shared.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClienteVO {

	private Number rut;
	private String dv;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String telefonoContacto;
	private String email;
    private List<CuentaVO> cuentas;
	
    public Number getRut() {
		return rut;
	}
	public void setRut(Number rut) {
		this.rut = rut;
	}
	public String getDv() {
		return dv;
	}
	public void setDv(String dv) {
		this.dv = dv;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getTelefonoContacto() {
		return telefonoContacto;
	}
	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<CuentaVO> getCuentas() {
		return cuentas;
	}
	public void setCuentas(List<CuentaVO> cuentas) {
		this.cuentas = cuentas;
	}

    
}
