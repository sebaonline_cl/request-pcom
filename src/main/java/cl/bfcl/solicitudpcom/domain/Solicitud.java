/**
 * 
 */
package cl.bfcl.solicitudpcom.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import cl.bfcl.solicitudpcom.domain.EstadoSolicitud;
import cl.bfcl.solicitudpcom.domain.SolicitudCliente;

// import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;


/**
 * @author ggonzalezh
 *
 */
@Data
@Entity
@Table(name= "SOLICITUD", schema = "hipotecaasicom")
public class Solicitud implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "numero")
	private BigDecimal nroSolicitud;   
	
	@Column(name = "tiposolicitud")
    private int tipoSolicitud;
    
	@Temporal(TemporalType.DATE)
	@Column(name = "fechaultimaactualizacion")
    private Date fechaUltimaActualizacion;
    
	@Temporal(TemporalType.DATE)
	@Column(name = "fecharatificacion")
    private Date fechaRatificacion;
    
	@Temporal(TemporalType.DATE)
	@Column(name = "fechaenvioasicom")
    private Date fechaEnvioAsicom;
    
	@ManyToOne
    @JoinColumn(name="estado",referencedColumnName = "codigo")	
    private EstadoSolicitud estadoSolicitud;

	
	@OneToOne
    @JoinColumn(name = "numero", referencedColumnName = "solicitud")
    private SolicitudCliente cliente;
	
	public Solicitud() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BigDecimal getNroSolicitud() {
		return nroSolicitud;
	}

	public void setNroSolicitud(BigDecimal nroSolicitud) {
		this.nroSolicitud = nroSolicitud;
	}

	public int getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(int tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getFechaRatificacion() {
		return fechaRatificacion;
	}

	public void setFechaRatificacion(Date fechaRatificacion) {
		this.fechaRatificacion = fechaRatificacion;
	}

	public Date getFechaEnvioAsicom() {
		return fechaEnvioAsicom;
	}

	public void setFechaEnvioAsicom(Date fechaEnvioAsicom) {
		this.fechaEnvioAsicom = fechaEnvioAsicom;
	}

	public EstadoSolicitud getEstadoSolicitud() {
		return estadoSolicitud;
	}

	public void setEstadoSolicitud(EstadoSolicitud estadoSolicitud) {
		this.estadoSolicitud = estadoSolicitud;
	}

	public SolicitudCliente getCliente() {
		return cliente;
	}

	public void setCliente(SolicitudCliente cliente) {
		this.cliente = cliente;
	}


	
	


    
}
