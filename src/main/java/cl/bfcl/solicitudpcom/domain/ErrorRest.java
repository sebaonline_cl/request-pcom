/**
 * 
 */
package cl.bfcl.solicitudpcom.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;


/**
 * @author ggonzalezh
 *
 */
public class ErrorRest {
	 @JsonProperty("code")
	  private String code = null;

	  @JsonProperty("message")
	  private String message = null;
	  
	  
	  public ErrorRest(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public ErrorRest code(String code) {
	    this.code = code;
	    return this;
	  }

	  /**
	   * Get code
	   * @return code
	  **/
	  @ApiModelProperty(required = true, value = "")
	  @NotNull

	  public String getCode() {
	    return code;
	  }

	  public void setCode(String code) {
	    this.code = code;
	  }

	  public ErrorRest message(String message) {
	    this.message = message;
	    return this;
	  }

	  /**
	   * Get message
	   * @return message
	  **/
	  @ApiModelProperty(required = true, value = "")
	  @NotNull

	  public String getMessage() {
	    return message;
	  }

	  public void setMessage(String message) {
	    this.message = message;
	  }


	  @Override
	  public boolean equals(java.lang.Object o) {
	    if (this == o) {
	      return true;
	    }
	    if (o == null || getClass() != o.getClass()) {
	      return false;
	    }
	    ErrorRest error = (ErrorRest) o;
	    return Objects.equals(this.code, error.code) &&
	        Objects.equals(this.message, error.message);
	  }

	  @Override
	  public int hashCode() {
	    return Objects.hash(code, message);
	  }

	  @Override
	  public String toString() {
	    StringBuilder sb = new StringBuilder();
	    sb.append("class Error {\n");
	    
	    sb.append("    code: ").append(toIndentedString(code)).append("\n");
	    sb.append("    message: ").append(toIndentedString(message)).append("\n");
	    sb.append("}");
	    return sb.toString();
	  }

	  /**
	   * Convert the given object to string with each line indented by 4 spaces
	   * (except the first line).
	   */
	  private String toIndentedString(java.lang.Object o) {
	    if (o == null) {
	      return "null";
	    }
	    return o.toString().replace("\n", "\n    ");
	  }

}
