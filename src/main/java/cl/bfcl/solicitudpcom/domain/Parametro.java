/**
 * 
 */
package cl.bfcl.solicitudpcom.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * @author ralvaradom
 *
 */
@Data
@Entity
@Table(name = "parametro", schema = "SGT")
public class Parametro {

    @Id
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHAPROCESO")
    private Date fechaProceso;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHACALENDARIO")
    private Date fechaCalendario;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_CONTABLE")
    private Date fechaContable;

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public Date getFechaCalendario() {
        return fechaCalendario;
    }

    public void setFechaCalendario(Date fechaCalendario) {
        this.fechaCalendario = fechaCalendario;
    }

    public Date getFechaContable() {
        return fechaContable;
    }

    public void setFechaContable(Date fechaContable) {
        this.fechaContable = fechaContable;
    }
}
