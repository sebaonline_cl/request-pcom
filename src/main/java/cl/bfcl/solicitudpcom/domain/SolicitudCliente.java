/**
 * 
 */
package cl.bfcl.solicitudpcom.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cl.bfcl.solicitudpcom.domain.Solicitud;

import lombok.Data;

/**
 * @author ggonzalezh
 *
 */
@Data
@Entity
@Table (name = "solicitudcliente" , schema = "hipotecaasicom")
public class SolicitudCliente {

	@Id
	@Column(name = "solicitud")
	private BigDecimal nroSolicitud;
	
    private long  rut;
    
    @Column(name = "dv", length = 1)
    private String dv;
    
    private String nombres;
    
    @Column(name = "apellidopaterno", length = 20)
    private String apellidoPaterno;
    
    @Column(name = "apellidomaterno", length = 20)
    private String apellidoMaterno;
    
    @Column(name = "telefonocontacto" , length = 20)
    private String telefonoContacto;
    
    @Column(name = "mail" , length = 50)
    private String email;
    
    @OneToOne(mappedBy = "cliente")
    @JsonIgnore
    private Solicitud solicitud;

	public BigDecimal getNroSolicitud() {
		return nroSolicitud;
	}

	public void setNroSolicitud(BigDecimal nroSolicitud) {
		this.nroSolicitud = nroSolicitud;
	}

	public long getRut() {
		return rut;
	}

	public void setRut(long rut) {
		this.rut = rut;
	}

	public String getDv() {
		return dv;
	}

	public void setDv(String dv) {
		this.dv = dv;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getTelefonoContacto() {
		return telefonoContacto;
	}

	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}
    
    
}
