/**
 * 
 */
package cl.bfcl.solicitudpcom.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


/**
 * @author ggonzalezh
 *
 */
@Data

@Entity
@Table(name = "estadosolicitud", schema = "hipotecaasicom")
public class EstadoSolicitud {
	
	@Id
	@Column(name = "codigo")
	private int codEstado;
	
	@Column(name = "descripcion")
	private String descEstado;
	
	@OneToMany(mappedBy = "estadoSolicitud")
	@JsonIgnore
    private Set<Solicitud> solicitud;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstadoSolicitud other = (EstadoSolicitud) obj;
		if (codEstado != other.codEstado)
			return false;
		if (descEstado == null) {
			if (other.descEstado != null)
				return false;
		} else if (!descEstado.equals(other.descEstado))
			return false;
		if (solicitud == null) {
			if (other.solicitud != null)
				return false;
		} else if (!solicitud.equals(other.solicitud))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codEstado;
		result = prime * result + ((descEstado == null) ? 0 : descEstado.hashCode());
		result = prime * result + ((solicitud == null) ? 0 : solicitud.hashCode());
		return result;
	}
	
	
	
}
